# Mirrors

List of git projects mirrored for csmOS.

## OpenEmbedded/Yocto core repositories

| Site | Repo |
| ---- | ---- |
| [bitbake](https://git.openembedded.org/bitbake) | `git://git.openembedded.org/bitbake.git` |
| [meta-openembedded](http://git.openembedded.org/meta-openembedded) | `git://git.openembedded.org/meta-openembedded.git` |
| [openembedded-core](https://git.openembedded.org/openembedded-core) | `git://git.openembedded.org/openembedded-core.git` |
| [poky](http://git.yoctoproject.org/cgit/cgit.cgi/poky) | `git://git.yoctoproject.org/poky.git` |


## OpenEmbedded/Yocto layers repositories

| Site | Repo |
| ---- | ---- |
| [meta-96boards](https://github.com/96boards/meta-96boards) | `git@github.com:96boards/meta-96boards.git` |
| [meta-meson](https://github.com/superna9999/meta-meson) | `git@github.com:superna9999/meta-meson.git` |
| [meta-qcom](http://git.yoctoproject.org/cgit/cgit.cgi/meta-qcom) | `git://git.yoctoproject.org/meta-qcom.git` |
| [meta-rpb](https://github.com/96boards/meta-rpb) | `git@github.com:96boards/meta-rpb.git` |
| [meta-virtualization](http://git.yoctoproject.org/cgit/cgit.cgi/meta-virtualization) | `git://git.yoctoproject.org/meta-virtualization` |
| [meta-clang](https://github.com/kraj/meta-clang) | `https://github.com/kraj/meta-clang.git` |


## Linux kernel repositories

| Site | Repo |
| ---- | ---- |
| https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git | `git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git` |
